package io.saul.martin.game.blade.player;

import io.saul.martin.game.blade.card.PlayableCard;
import io.saul.martin.game.blade.state.MyGameState;

import java.util.ArrayList;

public class StandardPlayer extends Player {
	@Override
	public PlayableCard move(MyGameState gameState) {
		if (!gameState.getMyCardsInPlay().get(gameState.getMyCardsInPlay().size() - 1).isActive()) {
			PlayableCard card = searchFor('1', gameState.getMyHand());
			if (card != null)
				return card;
		}

		int diff = gameState.getOpponentSumOfCardsInPlay() - gameState.getSumOfMyCardsInPlay();

		PlayableCard recCard = searchBetterThan(diff, gameState.getMyHand());
		if (recCard == null)
			recCard = searchFor('B', gameState.getMyHand());
		if (recCard == null)
			recCard = searchFor('M', gameState.getMyHand());
		if (recCard == null)
			recCard = searchFor('T', gameState.getMyHand());
		if (recCard == null)
			recCard = searchFor('K', gameState.getMyHand());

		return recCard;
	}

	protected PlayableCard searchBetterThan(int diff, ArrayList<PlayableCard> cardsInHand) {
		PlayableCard same = null;
		for (PlayableCard card : cardsInHand) {
			if (card.getValue() > diff)
				return card;
			if (card.getValue() == diff)
				same = card;
		}
		return same;
	}

}