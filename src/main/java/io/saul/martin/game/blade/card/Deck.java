package io.saul.martin.game.blade.card;

import java.util.LinkedList;

public class Deck {

	private final LinkedList<Card> cards;

	public Deck(LinkedList<Card> cards) {
		this.cards = cards;
	}

	public Deck splitStack() {
		LinkedList<Card> result = new LinkedList<>();

		while (result.size() < cards.size()) {
			result.add(cards.remove());
		}

		return new Deck(result);
	}

	public Card popCard() {
		return cards.pop();
	}

	public boolean hasCards() {
		return !cards.isEmpty();
	}

	public int size() {
		return cards.size();
	}
}
