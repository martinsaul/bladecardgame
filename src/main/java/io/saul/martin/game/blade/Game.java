package io.saul.martin.game.blade;

import io.saul.martin.game.blade.card.PlayableCard;
import io.saul.martin.game.blade.listener.GameListener;
import io.saul.martin.game.blade.listener.GameListenerManager;
import io.saul.martin.game.blade.listener.enumeration.VictoryCondition;
import io.saul.martin.game.blade.player.Player;
import io.saul.martin.game.blade.state.CompleteGameState;
import io.saul.martin.game.blade.table.Seat;
import io.saul.martin.game.blade.table.TableTop;

import java.util.HashMap;

public class Game {

	private final TableTop tableTop;

	private final GameListenerManager listener;
	private final HashMap<Seat, Seat> oppositeMap;

	public Game(Player playerA, Player playerB) {
		this.listener = new GameListenerManager();
		this.oppositeMap = new HashMap<>();
		this.tableTop = new TableTop(playerA, playerB);

		oppositeMap.put(tableTop.getSideA(), tableTop.getSideB());
		oppositeMap.put(tableTop.getSideB(), tableTop.getSideA());
	}

	public Player gameFlow() {
		Seat lastSeatToPlay = null;
		tableTop.wipeBoard();
		while (true) {

			Seat currentSeat = tableTop.getSeatWithLowestHand();
			Seat opponentSeat = oppositeMap.get(currentSeat);

			Player currentPlayer = currentSeat.getPlayer();

			CompleteGameState completeGameState = new CompleteGameState(currentSeat, opponentSeat);
			listener.gameState(completeGameState);

			if (tableTop.isTie()) {
				listener.tie();
				tableTop.wipeBoard();
				lastSeatToPlay = null;
				continue;
			}

			if (currentSeat == lastSeatToPlay) {
				listener.winner(opponentSeat, currentSeat, VictoryCondition.CouldNotBeat);
				return opponentSeat.getPlayer();
			}
			if (currentSeat.getCardsInHand().isEmpty()) {
				listener.winner(opponentSeat, currentSeat, VictoryCondition.OutOfCards);
				return opponentSeat.getPlayer();
			}

			listener.playersTurn(currentPlayer);

			PlayableCard selectedCard = currentPlayer.move(completeGameState.toMine());
			if (selectedCard == null) {
				listener.winner(opponentSeat, currentSeat, VictoryCondition.GivenUp);
				return opponentSeat.getPlayer();
			}

			tableTop.playerAction(currentSeat, selectedCard);

			listener.cardPlayed(currentSeat, selectedCard);

			lastSeatToPlay = currentSeat;
		}
	}

	public boolean registerGameListener(GameListener listener){
		return this.listener.registerListener(listener);
	}
}
