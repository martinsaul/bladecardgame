package io.saul.martin.game.blade;

import io.saul.martin.game.blade.listener.ConsoleGameListener;
import io.saul.martin.game.blade.player.ConsolePlayer;
import io.saul.martin.game.blade.player.Player;
import io.saul.martin.game.blade.player.StandardPlayer;

import java.util.Scanner;

public class Launcher {
	public static void main(String[] args){
		ConsolePlayer consolePlayer = new ConsolePlayer();

		int playerVictories = 0, games = 0;

		while(true) {
			Game game = new Game(consolePlayer, new StandardPlayer());
			game.registerGameListener(new ConsoleGameListener(consolePlayer));
			Player winner = game.gameFlow();
			games++;
			if(winner == consolePlayer) {
				playerVictories++;
				System.out.println("You win!");
			}
			System.out.printf("Game over! Player Victories: %d over %d games.%n", playerVictories, games);

			Scanner scanner = new Scanner(System.in);
			System.out.println("Play again? (y/N)");
			if(scanner.next().toUpperCase().charAt(0) != 'Y')
				return;
		}
	}
}
