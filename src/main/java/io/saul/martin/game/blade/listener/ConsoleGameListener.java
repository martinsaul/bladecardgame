package io.saul.martin.game.blade.listener;

import io.saul.martin.game.blade.card.PlayableCard;
import io.saul.martin.game.blade.listener.enumeration.VictoryCondition;
import io.saul.martin.game.blade.player.Player;
import io.saul.martin.game.blade.state.GameState;
import io.saul.martin.game.blade.state.ImmutableState;
import io.saul.martin.game.blade.table.Seat;

import java.text.MessageFormat;

public class ConsoleGameListener extends GameListener {
	public ConsoleGameListener(Player consolePlayer) {
		super(consolePlayer);
	}

	@Override
	public void tie() {
		System.out.println("Tie. Wiping board.");
	}

	@Override
	public void winner(Seat winner, Seat loser, VictoryCondition victoryCondition) {
		switch (victoryCondition){
			case GivenUp:
				System.out.printf("%s has given up.%n", loser.getPlayer());
				break;
			case OutOfCards:
				System.out.println("Player ran out of cards.");
				break;
			case CouldNotBeat:
				System.out.println("Player could not beat opponent card.");
				break;
			default:
		}
	}

	@Override
	public void cardPlayed(Seat currentSeat, PlayableCard selectedCard) {
		System.out.println(MessageFormat.format("Current Seat: {0} - Card Played: {1}", currentSeat.getPlayer(), selectedCard));
	}

	@Override
	public void gameState(GameState gameState) {
		ImmutableState state = (ImmutableState)gameState;
		System.out.println();
		System.out.println(MessageFormat.format("{0} Card in game: {1} (Sum: {2})", state.getMe(), state.getMyCardsInPlay(), state.getSumOfMyCardsInPlay()));
		System.out.println(MessageFormat.format("{0} Card in game: {1} (Sum: {2})", state.getOtherPlayer(), state.getOpponentCardsInPlay(), state.getOpponentSumOfCardsInPlay()));
		System.out.println(MessageFormat.format("Your hand: {0}", state.getMyHand()));
		System.out.println(MessageFormat.format("Opponent has {0} card(s) left", state.getOpponentCardCount()));
	}

	@Override
	public void playersTurn(Player player) {
		System.out.println(player + " turn!");
	}
}
