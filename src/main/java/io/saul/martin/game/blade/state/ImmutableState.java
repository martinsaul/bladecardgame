package io.saul.martin.game.blade.state;

import io.saul.martin.game.blade.card.PlayableCard;
import io.saul.martin.game.blade.player.Player;

import java.util.ArrayList;

public abstract class ImmutableState extends GameState{
	private final Player me, otherPlayer;
	private final ArrayList<PlayableCard> myHand;
	private final int opponentCardCount;

	ImmutableState(Player currentPlayer, Player opposingPlayer, ArrayList<PlayableCard> cardsInPlay, int sumOfCardsInPlay, ArrayList<PlayableCard> opponentCardsInPlay, int opponentSumOfCardsInPlay, ArrayList<PlayableCard> myHand, int opponentCardCount, Player me, Player otherPlayer){
		super(currentPlayer, opposingPlayer, cardsInPlay, sumOfCardsInPlay, opponentCardsInPlay, opponentSumOfCardsInPlay);
		this.myHand = myHand;
		this.opponentCardCount = opponentCardCount;
		this.me = me;
		this.otherPlayer = otherPlayer;
	}

	public ArrayList<PlayableCard> getMyHand() {
		return myHand;
	}

	public int getOpponentCardCount() {
		return opponentCardCount;
	}

	public Player getMe() {
		return me;
	}

	public Player getOtherPlayer() {
		return otherPlayer;
	}
}
