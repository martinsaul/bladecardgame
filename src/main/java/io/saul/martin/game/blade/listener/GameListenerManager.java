package io.saul.martin.game.blade.listener;

import io.saul.martin.game.blade.card.PlayableCard;
import io.saul.martin.game.blade.listener.enumeration.VictoryCondition;
import io.saul.martin.game.blade.player.Player;
import io.saul.martin.game.blade.state.CompleteGameState;
import io.saul.martin.game.blade.state.GameState;
import io.saul.martin.game.blade.table.Seat;

import java.util.ArrayList;

public class GameListenerManager extends GameListener {
	private final ArrayList<GameListener> listeners;

	public boolean registerListener(GameListener listener){
		return listeners.add(listener);
	}

	public GameListenerManager() {
		listeners = new ArrayList<>();
	}

	@Override
	public void tie() {
		for(GameListener listener: listeners)
			listener.tie();
	}

	@Override
	public void winner(Seat winner, Seat loser, VictoryCondition victoryCondition) {
		for(GameListener listener: listeners)
			listener.winner(winner,loser,victoryCondition);
	}

	@Override
	public void cardPlayed(Seat currentSeat, PlayableCard selectedCard) {
		for(GameListener listener: listeners)
			listener.cardPlayed(currentSeat, selectedCard);
	}

	@Override
	public void gameState(GameState gameState) {
		for(GameListener listener: listeners) {
			CompleteGameState complete = (CompleteGameState) gameState;
			if(gameState.getCurrentPlayer() == listener.getPlayer())
				listener.gameState(complete.toMine());
			else
				listener.gameState(complete.toOpponent());
		}
	}

	@Override
	public void playersTurn(Player player) {
		for (GameListener listener: listeners)
			listener.playersTurn(player);
	}
}
