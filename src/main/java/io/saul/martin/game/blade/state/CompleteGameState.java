package io.saul.martin.game.blade.state;

import io.saul.martin.game.blade.card.PlayableCard;
import io.saul.martin.game.blade.table.Seat;

import java.util.ArrayList;

public class CompleteGameState extends GameState{
	private final ArrayList<PlayableCard> currentSeatHand;
	private final ArrayList<PlayableCard> opponentSeatHand;

	public CompleteGameState(Seat currentSeat, Seat opponentSeat){
		super(currentSeat.getPlayer(), opponentSeat.getPlayer(), currentSeat.getCardsInPlay(), currentSeat.getSumOfCardsInPlay(), opponentSeat.getCardsInPlay(), opponentSeat.getSumOfCardsInPlay());
		this.currentSeatHand = currentSeat.getCardsInHand();
		this.opponentSeatHand = opponentSeat.getCardsInHand();
	}

	public MyGameState toMine() {
		return new MyGameState(this);
	}

	public GameState toOpponent() {
		return new OpponentGameState(this);
	}

	ArrayList<PlayableCard> getCurrentSeatHand() {
		return currentSeatHand;
	}

	ArrayList<PlayableCard> getOpponentSeatHand() {
		return opponentSeatHand;
	}
}
