package io.saul.martin.game.blade.card;

import java.util.Objects;

public class Card implements Comparable<Card> {

	private final Character symbol;
	private final Effect effect;
	private final int value;

	public Card(Character symbol, Effect effect) {
		this.symbol = symbol;
		this.effect = effect;
		this.value = parseSymbol(symbol);
	}

	public Character getSymbol() {
		return symbol;
	}

	public Effect getEffect() {
		return effect;
	}

	public int getValue() {
		return value;
	}

	@Override
	public String toString() {
		return symbol.toString();
	}

	private int parseSymbol(Character symbol) {
		//TODO This solution is ugly and it bothers me Java seems to be unable to give a decent native solution that doesn't throw an exception.
		try {
			return Integer.parseInt(symbol.toString());
		} catch (NumberFormatException ex) {
			return 1;
		}
	}

	@Override
	public int compareTo(Card o) {
		return Integer.compare(getSortValue(), o.getSortValue());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Card card = (Card) o;
		return value == card.value &&
				Objects.equals(symbol, card.symbol) &&
				effect == card.effect;
	}

	@Override
	public int hashCode() {
		return Objects.hash(symbol, effect, value);
	}

	public int getSortValue() {
		if (effect != Effect.None && symbol != '1')
			return Integer.MAX_VALUE;
		return value;
	}
}
