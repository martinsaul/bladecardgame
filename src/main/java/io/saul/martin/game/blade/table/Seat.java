package io.saul.martin.game.blade.table;

import io.saul.martin.game.blade.card.Card;
import io.saul.martin.game.blade.card.Deck;
import io.saul.martin.game.blade.card.PlayableCard;
import io.saul.martin.game.blade.player.Player;

import java.util.ArrayList;

public class Seat {
	private final Deck deck;
	private final Player player;
	private final Hand hand;
	private final ArrayList<PlayableCard> cardsInPlay;

	public Seat(Deck deck, Player player) {
		this.deck = deck;
		this.player = player;
		cardsInPlay = new ArrayList<>();
		hand = new Hand(10);

		hand.load(deck);
	}

	public void wipeCardsInPlay(Card card) {
		cardsInPlay.clear();
		cardsInPlay.add(new PlayableCard(card));
	}

	public int getSumOfCardsInPlay() {
		int count = 0;

		for (PlayableCard card : cardsInPlay) {
			if (card.isActive())
				count += card.getValue();
		}

		return count;
	}

	public ArrayList<PlayableCard> getCardsInHand() {
		return hand.getCardsInHand();
	}

	public void playCard(PlayableCard card, boolean cardExhausted) {
		if (hand.hasCard(card)) {
			hand.useCard(card);
			if (cardExhausted)
				cardsInPlay.add(card);
		}
	}

	public Deck getDeck() {
		return deck;
	}

	public PlayableCard getLastCard() {
		if (!cardsInPlay.isEmpty())
			return cardsInPlay.get(cardsInPlay.size() - 1);
		return null;
	}

	public ArrayList<PlayableCard> getCardsInPlay() {
		return new ArrayList<>(cardsInPlay);
	}

	public void changeCardsInPlay(ArrayList<PlayableCard> newCards) {
		cardsInPlay.clear();
		cardsInPlay.addAll(newCards);
	}

	public void draw(int count) {
		for(int i = 0; i < count && deck.hasCards(); i++)
			hand.add(deck.popCard());
	}

	public Player getPlayer() {
		return player;
	}
}
