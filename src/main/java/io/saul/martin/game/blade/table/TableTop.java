package io.saul.martin.game.blade.table;

import io.saul.martin.game.blade.card.CardEngine;
import io.saul.martin.game.blade.card.Deck;
import io.saul.martin.game.blade.card.PlayableCard;
import io.saul.martin.game.blade.player.Player;

public class TableTop {
	private final Seat sideA;
	private final Seat sideB;

	public TableTop(Player playerA, Player playerB) {
		Deck deck = new CardEngine().buildDeck();
		sideA = new Seat(deck, playerA);
		sideB = new Seat(deck.splitStack(), playerB);
	}

	public Seat getSideA() {
		return sideA;
	}

	public Seat getSideB() {
		return sideB;
	}

	public void playerAction(Seat currentSeat, PlayableCard playableCard) {
		Seat opponent = (currentSeat == sideA) ? sideB : sideA;

		if (playableCard != null) {
			boolean cardExhausted = playableCard.getEffect().execute(this, currentSeat, opponent, playableCard);
			currentSeat.playCard(playableCard, cardExhausted);
		}
	}

	public boolean isTie() {
		return sideA.getSumOfCardsInPlay() == sideB.getSumOfCardsInPlay();
	}

	public void wipeBoard() {
		sideA.wipeCardsInPlay(sideA.getDeck().popCard());
		sideB.wipeCardsInPlay(sideB.getDeck().popCard());
	}

	public Seat getSeatWithLowestHand() {
		int sideACount = sideA.getSumOfCardsInPlay();
		int sideBCount = sideB.getSumOfCardsInPlay();

		return (sideACount > sideBCount) ? sideB : sideA;
	}
}
