package io.saul.martin.game.blade.state;

public final class MyGameState extends ImmutableState{
	MyGameState(CompleteGameState state){
		super(
				state.getCurrentPlayer(),
				state.getOpposingPlayer(),
				state.getMyCardsInPlay(),
				state.getSumOfMyCardsInPlay(),
				state.getOpponentCardsInPlay(),
				state.getOpponentSumOfCardsInPlay(),
				state.getCurrentSeatHand(),
				state.getOpponentSeatHand().size(),
				state.getCurrentPlayer(),
				state.getOpposingPlayer()
		);
	}
}
