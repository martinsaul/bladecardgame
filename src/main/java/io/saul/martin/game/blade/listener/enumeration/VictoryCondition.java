package io.saul.martin.game.blade.listener.enumeration;

public enum VictoryCondition {
	OutOfCards, GivenUp, CouldNotBeat
}
