package io.saul.martin.game.blade.card;

import java.text.MessageFormat;

public class PlayableCard implements Comparable<PlayableCard> {
	private final Card card;
	private boolean active;
	private Integer modifiedValue;

	public PlayableCard(Card card) {
		this.card = card;
		this.active = true;
		this.modifiedValue = null;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Character getSymbol() {
		return card.getSymbol();
	}

	public Effect getEffect() {
		return card.getEffect();
	}

	public int getValue() {
		if(modifiedValue != null)
			return modifiedValue;
		return card.getValue();
	}

	public void setModifiedValue(Integer value){
		this.modifiedValue = value;
	}

	@Override
	public String toString() {
		String pattern = (active) ? "{0}" : "({0})";
		return MessageFormat.format(pattern, card.toString());
	}

	@Override
	public int compareTo(PlayableCard o) {
		return Integer.compare(card.getSortValue(), o.card.getSortValue());
	}

	public boolean isCard(Card card) {
		return this.card.equals(card);
	}
}
