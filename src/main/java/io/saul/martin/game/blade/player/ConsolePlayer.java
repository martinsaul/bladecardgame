package io.saul.martin.game.blade.player;

import io.saul.martin.game.blade.card.PlayableCard;
import io.saul.martin.game.blade.state.MyGameState;

import java.util.Scanner;

public class ConsolePlayer extends Player {
	public PlayableCard move(MyGameState gameState) {
		Scanner scanner = new Scanner(System.in);

		PlayableCard card = null;

		while (card == null) {
			System.out.println("Please play a card:");
			String command = scanner.next();

			if (command.length() == 1) {
				card = searchFor(command.toUpperCase().charAt(0), gameState.getMyHand());
			} else {
				if(command.equalsIgnoreCase("quit")){
					break;
				}
			}

		}

		return card;
	}
}
