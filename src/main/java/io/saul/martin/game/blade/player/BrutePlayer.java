package io.saul.martin.game.blade.player;

import io.saul.martin.game.blade.card.PlayableCard;
import io.saul.martin.game.blade.state.MyGameState;

import java.util.ArrayList;

public class BrutePlayer extends StandardPlayer {

	@Override
	protected PlayableCard searchBetterThan(int diff, ArrayList<PlayableCard> cardsInHand) {
		PlayableCard same = null;
		for (int i = cardsInHand.size() - 1; i >= 0; i--) {
			PlayableCard card = cardsInHand.get(i);
			if (card.getValue() > diff)
				return card;
			if (card.getValue() == diff)
				same = card;
		}
		return same;
	}

}