package io.saul.martin.game.blade.player;

import io.saul.martin.game.blade.card.PlayableCard;
import io.saul.martin.game.blade.state.MyGameState;

import java.util.ArrayList;
import java.util.Random;

@SuppressWarnings("unused")
public class RandomPlayer extends Player {

	private final Random random;

	public RandomPlayer() {
		random = new Random();
	}

	@Override
	public PlayableCard move(MyGameState gameState) {
		if(random.nextBoolean()) {
			if (!gameState.getMyCardsInPlay().get(gameState.getMyCardsInPlay().size() - 1).isActive()) {
				PlayableCard card = searchFor('1', gameState.getMyHand());
				if (card != null)
					return card;
			}
		}

		int diff = gameState.getOpponentSumOfCardsInPlay() - gameState.getSumOfMyCardsInPlay();

		ArrayList<PlayableCard> possibleCards = searchBetterThan(diff, gameState.getMyHand());

		if(possibleCards.isEmpty())
			return null;

		return possibleCards.get(Math.abs(random.nextInt())%possibleCards.size());
	}

	private ArrayList<PlayableCard> searchBetterThan(int diff, ArrayList<PlayableCard> cardsInHand) {
		ArrayList<PlayableCard> result = new ArrayList<>();
		for (PlayableCard card : cardsInHand) {
			if (card.getValue() >= diff)
				result.add(card);
		}
		return result;
	}

}