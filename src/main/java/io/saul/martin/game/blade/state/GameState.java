package io.saul.martin.game.blade.state;

import io.saul.martin.game.blade.card.PlayableCard;
import io.saul.martin.game.blade.player.Player;

import java.util.ArrayList;

public abstract class GameState {

	private final Player currentPlayer;
	private final Player opposingPlayer;

	private final ArrayList<PlayableCard> myCardsInPlay;
	private final int sumOfMyCardsInPlay;
	private final ArrayList<PlayableCard> opponentCardsInPlay;
	private final int opponentSumOfCardsInPlay;

	GameState(Player currentPlayer, Player opposingPlayer, ArrayList<PlayableCard> myCardsInPlay, int sumOfMyCardsInPlay, ArrayList<PlayableCard> opponentCardsInPlay, int opponentSumOfCardsInPlay){
		this.currentPlayer = currentPlayer;
		this.myCardsInPlay = myCardsInPlay;
		this.sumOfMyCardsInPlay = sumOfMyCardsInPlay;
		this.opposingPlayer = opposingPlayer;
		this.opponentCardsInPlay = opponentCardsInPlay;
		this.opponentSumOfCardsInPlay = opponentSumOfCardsInPlay;
	}

	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	@SuppressWarnings("WeakerAccess")
	public Player getOpposingPlayer() {
		return opposingPlayer;
	}

	public ArrayList<PlayableCard> getMyCardsInPlay() {
		return myCardsInPlay;
	}

	public int getSumOfMyCardsInPlay() {
		return sumOfMyCardsInPlay;
	}

	public ArrayList<PlayableCard> getOpponentCardsInPlay() {
		return opponentCardsInPlay;
	}

	public int getOpponentSumOfCardsInPlay() {
		return opponentSumOfCardsInPlay;
	}
}
