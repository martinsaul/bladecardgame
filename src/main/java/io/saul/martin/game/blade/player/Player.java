package io.saul.martin.game.blade.player;

import io.saul.martin.game.blade.card.PlayableCard;
import io.saul.martin.game.blade.state.MyGameState;

import java.text.MessageFormat;
import java.util.ArrayList;

public abstract class Player {

	private static int id = 1;
	private final String name;

	public Player() {
		this(MessageFormat.format("Player {0}", id));
		id++;
	}

	public Player(String name) {
		this.name = name;
	}

	public abstract PlayableCard move(MyGameState gameState);

	@Override
	public String toString() {
		return name;
	}

	@SuppressWarnings("WeakerAccess")
	protected PlayableCard searchFor(char c, ArrayList<PlayableCard> cardsInHand) {
		for (PlayableCard card : cardsInHand)
			if (card.getSymbol() == c)
				return card;
		return null;
	}
}
