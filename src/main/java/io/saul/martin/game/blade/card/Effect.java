package io.saul.martin.game.blade.card;

import io.saul.martin.game.blade.table.Seat;
import io.saul.martin.game.blade.table.TableTop;

import java.util.ArrayList;

public enum Effect {
	Resurrect((TableTop state, Seat currentSeat, Seat opponent, PlayableCard currentCard) -> {
		PlayableCard lastCard = currentSeat.getLastCard();
		if (lastCard != null && !lastCard.isActive()) {
			lastCard.setActive(true);
			return false;
		}
		return true;
	}),
	None((TableTop state, Seat currentSeat, Seat opponent, PlayableCard currentCard) -> true),
	Mirror((TableTop state, Seat currentSeat, Seat opponent, PlayableCard currentCard) -> {
		ArrayList<PlayableCard> cardsInPlay = currentSeat.getCardsInPlay();
		ArrayList<PlayableCard> cardsInOpponentPlay = opponent.getCardsInPlay();
		currentSeat.changeCardsInPlay(cardsInOpponentPlay);
		opponent.changeCardsInPlay(cardsInPlay);
		return false;
	}),
	Blade((TableTop state, Seat currentSeat, Seat opponent, PlayableCard currentCard) -> {
		PlayableCard lastCard = opponent.getLastCard();
		if (lastCard != null && lastCard.isActive()) {
			lastCard.setActive(false);
			return false;
		}
		return true;
	}), Topple((TableTop state, Seat currentSeat, Seat opponent, PlayableCard currentCard) -> {
		int value = opponent.getSumOfCardsInPlay() - currentSeat.getSumOfCardsInPlay();
		currentCard.setModifiedValue(value);
		return true;
	}), Draw2((TableTop state, Seat currentSeat, Seat opponent, PlayableCard currentCard) -> {
		state.wipeBoard();
		int value = opponent.getSumOfCardsInPlay() - currentSeat.getSumOfCardsInPlay() + 1;
		currentCard.setModifiedValue(value);
		currentSeat.draw(2);
		opponent.draw(2);
		return true;
	});

	private final EffectAction action;

	Effect(EffectAction action) {
		this.action = action;
	}

	public boolean execute(TableTop state, Seat currentSeat, Seat opponent, PlayableCard currentCard) {
		return action.action(state, currentSeat, opponent, currentCard);
	}

	private interface EffectAction {
		boolean action(TableTop state, Seat currentSeat, Seat opponent, PlayableCard currentCard);
	}
}
