package io.saul.martin.game.blade.state;

@SuppressWarnings("WeakerAccess")
public final class OpponentGameState extends ImmutableState{
	OpponentGameState(CompleteGameState state){
		super(
				state.getCurrentPlayer(),
				state.getOpposingPlayer(),
				state.getOpponentCardsInPlay(),
				state.getOpponentSumOfCardsInPlay(),
				state.getMyCardsInPlay(),
				state.getSumOfMyCardsInPlay(),
				state.getOpponentSeatHand(),
				state.getCurrentSeatHand().size(),
				state.getOpposingPlayer(),
				state.getCurrentPlayer()
		);
	}
}
