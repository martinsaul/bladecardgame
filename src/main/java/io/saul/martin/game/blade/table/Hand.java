package io.saul.martin.game.blade.table;

import io.saul.martin.game.blade.card.Card;
import io.saul.martin.game.blade.card.Deck;
import io.saul.martin.game.blade.card.PlayableCard;

import java.util.ArrayList;
import java.util.Collections;

public class Hand {

	private final ArrayList<PlayableCard> cards;
	private final int cap;

	public Hand(int cap) {
		this.cap = cap;
		cards = new ArrayList<>(cap);
	}

	public boolean useCard(PlayableCard card) {
		return cards.remove(card);
	}

	public ArrayList<PlayableCard> getCardsInHand() {
		return new ArrayList<>(cards);
	}

	public boolean isFull() {
		return cards.size() == cap;
	}

	public void load(Deck deck) {
		while (!isFull())
			add(deck.popCard());
	}

	public void add(Card card) {
		PlayableCard playableCard = new PlayableCard(card);
		int index = Collections.binarySearch(cards, playableCard);
		if (index < 0) index = ~index;
		cards.add(index, playableCard);
	}

	public boolean hasCard(PlayableCard card) {
		return cards.contains(card);
	}

	public PlayableCard getPlayableCard(Card card) {
		return cards.stream().filter(playableCard -> playableCard.isCard(card)).findFirst().orElse(null);
	}
}
