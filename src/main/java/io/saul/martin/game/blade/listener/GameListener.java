package io.saul.martin.game.blade.listener;

import io.saul.martin.game.blade.card.PlayableCard;
import io.saul.martin.game.blade.listener.enumeration.VictoryCondition;
import io.saul.martin.game.blade.player.Player;
import io.saul.martin.game.blade.state.GameState;
import io.saul.martin.game.blade.table.Seat;

public abstract class GameListener {
	private final Player player;

	GameListener() {
		this.player = null;
	}

	@SuppressWarnings("WeakerAccess")
	public GameListener(Player player) {
		if(player == null)
			throw new RuntimeException("Game listeners are not allowed to be instantiated without being associated to a player.");
		this.player = player;
	}

	public abstract void tie();

	public abstract void winner(Seat winner, Seat loser, VictoryCondition victoryCondition);

	public abstract void cardPlayed(Seat currentSeat, PlayableCard selectedCard);

	public abstract void gameState(GameState gameState);

	public Player getPlayer() {
		return player;
	}

	public abstract void playersTurn(Player player);
}
