package io.saul.martin.game.blade.card;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;

public class CardEngine {
	private static final Card[] officialCards = {
			new Card('1', Effect.Resurrect),
			new Card('2', Effect.None),
			new Card('3', Effect.None),
			new Card('4', Effect.None),
			new Card('5', Effect.None),
			new Card('6', Effect.None),
			new Card('7', Effect.None),
			new Card('8', Effect.None),
			new Card('9', Effect.None),
			new Card('B', Effect.Blade),
			new Card('M', Effect.Mirror),
			new Card('K', Effect.Draw2),
			new Card('T', Effect.Topple)
	};

	public Deck buildDeck() {
		ArrayList<Card> cards = buildAllCardsInSetsOf(8);

		LinkedList<Card> randomCards = new LinkedList<>();

		Random random = new Random();
		while (!cards.isEmpty()) {
			randomCards.add(cards.remove(Math.abs(random.nextInt() % cards.size())));
		}

		return new Deck(randomCards);
	}

	private ArrayList<Card> buildAllCardsInSetsOf(int i) {
		ArrayList<Card> cards = new ArrayList<>();
		for (int count = 0; count < i; count++)
			cards.addAll(Arrays.asList(officialCards));
		return cards;
	}

}
