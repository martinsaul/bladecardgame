package io.saul.martin.game.blade;

import io.saul.martin.game.blade.card.CardEngine;
import io.saul.martin.game.blade.card.Deck;
import org.junit.Test;

public class DeckTest {

	@Test
	public void splitStack() {
		Deck deck = new CardEngine().buildDeck();

		int size = deck.size();

		Deck newDeck = deck.splitStack();

		assert deck.size() == newDeck.size() && deck.size() + newDeck.size() == size;
	}
}