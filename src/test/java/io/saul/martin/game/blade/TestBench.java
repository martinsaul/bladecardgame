package io.saul.martin.game.blade;

import io.saul.martin.game.blade.listener.ConsoleGameListener;
import io.saul.martin.game.blade.player.StandardPlayer;
import org.junit.Test;

public class TestBench {

	@Test
	public void test() {

		StandardPlayer playerA = new StandardPlayer();
		Game game = new Game(playerA, new StandardPlayer());
		game.registerGameListener(new ConsoleGameListener(playerA));

		game.gameFlow();
	}
}
