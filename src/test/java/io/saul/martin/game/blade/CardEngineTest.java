package io.saul.martin.game.blade;

import io.saul.martin.game.blade.card.Card;
import io.saul.martin.game.blade.card.CardEngine;
import io.saul.martin.game.blade.card.Deck;
import org.junit.Test;

import java.util.HashMap;

public class CardEngineTest {

	@Test
	public void createsValidDeck() {
		Deck deck = new CardEngine().buildDeck();

		HashMap<Card, Integer> cardCount = new HashMap<>();

		while (deck.hasCards()) {
			Card currentCard = deck.popCard();
			cardCount.merge(currentCard, 1, (a, b) -> a + b);
		}

		validateCount(cardCount, 8);
	}

	private void validateCount(HashMap<Card, Integer> cardCount, int count) {
		for (Card card : cardCount.keySet()) {
			assert cardCount.get(card) == count;
		}
	}

}